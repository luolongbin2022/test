// 必填字段
let matching = (value,callback,reg,message) => {
    if(value === '' || value === undefined || value == null ){
        callback(new Error("这一项必填"))
    }else {
        if(!reg.test(value)){
            callback(new Error(message))
        }else {
            callback()
        }
    }
}
// 非必填字段
let matched = (value,callback,reg,message) => {
    if(value === '' || value === undefined || value == null ){
        callback()
    }else {
        if(!reg.test(value)){
            callback(new Error(message))
        }else {
            callback()
        }
    }
}
module.exports = {
    ipAddress(rule,value,callback){
        let reg = /^([0-9a-fA-F]{1,4}::?){1,7}([0-9a-fA-F]{1,4})?(\/([1-9]|[1-9]\d|1[01]\d|12[0-8]))?$/
        matching(value,callback,reg,"保存失败。IP地址:非法IPv6格式,示例:2001::1/64")
    },
    gateway(rule,value,callback){
        let reg = /^([0-9a-fA-F]{1,4}::?){1,7}([0-9a-fA-F]{1,4})?(\/(6[4-9]|[7-9]\d|1[01]\d|12[0-8]))?$/
        matching(value,callback,reg,"保存失败。网关:非法IPv6格式,示例:2001::1/64")
    },
    firstDns(rule,value,callback){
        let reg = /^([0-9a-fA-F]{1,4}::?){1,7}([0-9a-fA-F]{1,4})?(\/([1-9]|[1-9]\d|1[01]\d|12[0-8]))?$/
        matching(value,callback,reg,"保存失败。首选DNS:非法IPv6格式,示例:2001::1/64")
    },
    secondDns(rule,value,callback){
        let reg = /^([0-9a-fA-F]{1,4}::?){1,7}([0-9a-fA-F]{1,4})?(\/([1-9]|[1-9]\d|1[01]\d|12[0-8]))?$/
        matched(value,callback,reg,"保存失败。首选DNS:非法IPv6格式,示例:2001::1/64")
    },
    lanAddress(rule,value,callback){
        let reg = /^([0-9a-fA-F]{1,4}::?){1,7}([0-9a-fA-F]{1,4})?$/
        matching(value,callback,reg,"保存失败。地址:非法IPv6格式,示例:2001::1")
    },
    account(rule,value,callback){
        let reg = /^(\w{3,10})$/
        matching(value,callback,reg,"保存失败。账号3-10,不能。。。。")
    },
    password(rule,value,callback){
        let reg = /^(\w{3,10})$/
        matching(value,callback,reg,"保存失败。密码3-10,不能。。。。")
    }
}